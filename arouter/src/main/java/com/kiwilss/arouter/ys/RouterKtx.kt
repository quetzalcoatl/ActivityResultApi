package com.kiwilss.arouter.ys

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import com.alibaba.android.arouter.launcher.ARouter
import com.kiwilss.arouter.post.buildIntent
import com.kiwilss.arouter.post.pretreatment
import com.kiwilss.arouter.ys.ActivityHelperK

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/4
 * @desc   : {DESCRIPTION}
 */
object RouterKtx {

}

/**
 *不带参数跳转
 * @param path
 */
fun Context?.startActivityA(path: String) {
    ARouter.getInstance().build(path).navigation()
}

/**
 *带参数跳转使用 bundle
 * @param path
 * @param bundle
 */
fun Context?.startActivityA(path: String, bundle: Bundle) {
    ARouter.getInstance().build(path).with(bundle).navigation()
}

/**
 *跳转使用 pair 传参
 * @param path
 * @param pair
 */
fun Context?.startActivityA(
    path: String,
    vararg pair: Pair<String, Any?>
) {
    ARouter.getInstance().build(path).with(bundleOf(*pair)).navigation()
}

/**
 *回调跳转不带任何参数
 * @param path
 * @param callback
 */
fun Context?.startActivityForResultA(
    path: String,
    callback: ((Int, Intent?) -> Unit)? = null
) {
    if (this == null) return
    //先生成 postcard
    ARouter.getInstance().build(path).pretreatment(this)?.let {
        ActivityHelperK.init(this)
            .startActivityForResult(it.buildIntent(this), callback)
    }
}

/**
 *回调带参数跳转使用 bundle
 * @param path
 * @param bundle
 * @param callback
 */
fun Context?.startActivityForResultA(
    path: String, bundle: Bundle,
    callback: ((Int, Intent?) -> Unit)? = null
) {
    if (this == null) return
    //先生成 postcard
    ARouter.getInstance().build(path).with(bundle)
        .pretreatment(this)?.let {
            ActivityHelperK.init(this)
                .startActivityForResult(it.buildIntent(this), callback)
        }
}

/**
 *回调跳转使用 pair 传参
 * @param path
 * @param pair
 * @param callback
 */
fun Context?.startActivityForResultA(
    path: String,
    vararg pair: Pair<String, Any?>, callback: ((Int, Intent?) -> Unit)? = null
) {
    if (this == null) return
    ARouter.getInstance().build(path).with(bundleOf(*pair))
        .pretreatment(this)?.let {
            ActivityHelperK.init(this)
                .startActivityForResult(it.buildIntent(this), callback)
        }
}





