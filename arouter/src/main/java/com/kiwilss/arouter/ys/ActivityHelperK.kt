package com.kiwilss.arouter.ys

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity

class ActivityHelperK private constructor(val context: Context?) {
    private val TAG = "MMMActivityHelperK"
//    private var mContext: Activity? = null
    private var mRouterFragment: RouterFragmentK? = null


    init {
        if (context is FragmentActivity) {
            mRouterFragment = getRouterFragment(context)
        }
    }

    companion object {
        fun init(context: Context?): ActivityHelperK {
            return ActivityHelperK(context)
        }
    }


    private fun getRouterFragment(activity: FragmentActivity?): RouterFragmentK? {
        if (activity == null) return null
        var routerFragment: RouterFragmentK? = findRouterFragment(activity)
        if (routerFragment == null) {
            //创建 fragment,加入当前 activity
            routerFragment = RouterFragmentK.newInstance()
            val sfm = activity.supportFragmentManager
            sfm.beginTransaction().add(routerFragment, TAG).commitAllowingStateLoss()
            sfm.executePendingTransactions()
        }
        return routerFragment
    }

    private fun findRouterFragment(activity: FragmentActivity): RouterFragmentK? {
        //通过 tag 获取 fragment
        return activity.supportFragmentManager.findFragmentByTag(TAG) as RouterFragmentK?
    }

    /**
     * 针对阿里路由跳转,回调跳转
     */
    fun startActivityForResult(
        intent: Intent,
        callback: ((Int, Intent?) -> Unit)? = null
    ) {
        mRouterFragment?.run {
            startActivityForResult(intent, callback)
        }
    }


}