package com.kiwilss.arouter.post

import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/4
 * @desc   : {DESCRIPTION}
 */
object NoResult {
    const val tag = "result_fragment"

    //通过tag找到fragment
    private fun findOnResultFragment(activity: FragmentActivity): ResultFragment? =
        activity.supportFragmentManager.findFragmentByTag(tag) as? ResultFragment

    //在找不到fragment的时候创建新的fragment
    private fun getOnResultFragment(activity: FragmentActivity): ResultFragment {
        return findOnResultFragment(activity) ?: activity.supportFragmentManager.run {
            ResultFragment().apply {
                beginTransaction().add(this, NoResult.tag).commitAllowingStateLoss()
                executePendingTransactions()
            }
        }
    }

    fun startActivityForResult(
        fragment: Fragment, intent: Intent, requestCode: Int,
        callback: (requestCode: Int, resultCode: Int, data: Intent?) -> Unit
    ) {
        startActivityForResult(fragment.requireActivity(), intent, requestCode, callback)
    }

    fun startActivityForResult(
        activity: FragmentActivity, intent: Intent, requestCode: Int,
        callback: (requestCode: Int, resultCode: Int, data: Intent?) -> Unit
    ) {
        getOnResultFragment(activity).startActivityForResult(intent, requestCode, callback)
    }

}