# ActivityResultApi

## 前言
对 ARouter 封装和 ActivityResultApi 封装汇总，[ARouter封装](https://www.yuque.com/bibly/selhsz/fuvcq0)和[ActivityResultAPi](https://www.yuque.com/bibly/selhsz/bm05vl) 功能有类似的地方，都有跳转回调相关的方法，不过 ARouter 更偏向于组件间跳转，而 ActivityResultApi 除了跳转外，还有请求权限相关功能及谷歌新增获取联系人等方法，两个博客里面都有详细介绍，这篇主要是介绍如何导入。
## 两个都导入
最新版：

![](https://www.jitpack.io/v/com.gitee.quetzalcoatl/ActivityResultApi.svg)

```
Add it in your root build.gradle at the end of repositories:

	allprojects {
		repositories {
			...
			maven { url 'https://www.jitpack.io' }
		}
	}
Step 2. Add the dependency

	dependencies {
	        implementation 'com.gitee.quetzalcoatl.ActivityResultApi:0.0.4'
	}
```
## 只导入一个
- ARouter
```
implementation 'com.gitee.quetzalcoatl.ActivityResultApi:arouter:0.0.4'
```
- ActivityResultAPi

```
implementation 'com.gitee.quetzalcoatl.ActivityResultApi:api:0.0.4'
```
## 问题

遇到问题可以参考[ARouter封装](https://www.yuque.com/bibly/selhsz/fuvcq0)和[ActivityResultAPi](https://www.yuque.com/bibly/selhsz/bm05vl)






    
