package com.kiwilss.api.permission

import androidx.activity.ComponentActivity
import androidx.fragment.app.Fragment

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/1
 * @desc   : {DESCRIPTION}
 */
class ContractDsl {
}

inline fun Fragment.requestPermissionK(
    permission: String,
    builderPermission: PermissionBuilder.() -> Unit
) = run {
    val builder = PermissionBuilder()
    builder.apply(builderPermission)
    requestPermission(
        permission,
        granted = builder.granted,
        denied = builder.denied,
        explained = builder.explained
    )
}


inline fun Fragment.requestPermissionK(builderPermission: MultiPermissionBuilder.() -> Unit) = run {
    val builder = MultiPermissionBuilder()
    builder.apply(builderPermission)
    requestMultiplePermissions(
        allGranted = builder.allGranted,
        denied = builder.denied,
        explained = builder.explained
    )
}


inline fun ComponentActivity.requestPermissionK(
    permission: String,
    builderPermission: PermissionBuilder.() -> Unit
) = run {
    val builder = PermissionBuilder()
    builder.apply(builderPermission)
    requestPermission(
        permission,
        granted = builder.granted,
        denied = builder.denied,
        explained = builder.explained
    )
}

inline fun ComponentActivity.requestPermissionK(
    builderPermission: MultiPermissionBuilder.() -> Unit
) = run {
    val builder = MultiPermissionBuilder()
    builder.apply(builderPermission)
    requestMultiplePermissions(
        allGranted = builder.allGranted,
        denied = builder.denied,
        explained = builder.explained
    )
}


class PermissionBuilder {
    var granted: (permission: String) -> Unit = {}
    var denied: (permission: String) -> Unit = {}
    var explained: (permission: String) -> Unit = {}

    fun granted(granted: (permission: String) -> Unit = {}){
        this.granted = granted
    }

    fun denied(denied: (permission: String) -> Unit = {}){
        this.denied = denied
    }

    fun explained(explained: (permission: String) -> Unit = {}){
        this.explained = explained
    }

}


class MultiPermissionBuilder {
    var allGranted: () -> Unit = {}
    var denied: (List<String>) -> Unit = {}
    var explained: (List<String>) -> Unit = {}
    fun allGranted(allGranted: () -> Unit = {}) {
        this.allGranted = allGranted
    }

    fun denied(denied: (List<String>) -> Unit = {}) {
        this.denied = denied
    }

    fun explained(explained: (List<String>) -> Unit = {}) {
        this.explained = explained
    }
}