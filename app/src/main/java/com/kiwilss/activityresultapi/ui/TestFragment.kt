package com.kiwilss.activityresultapi.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import com.alibaba.android.arouter.launcher.ARouter
import com.kiwilss.activityresultapi.R
import com.kiwilss.activityresultapi.config.RouterPage
import com.kiwilss.api.startActivityK
import com.kiwilss.arouter.post.navigateForResult
import com.kiwilss.arouter.ys.startActivityA
import com.kiwilss.arouter.ys.startActivityForResultA

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/6
 * @desc   : {DESCRIPTION}
 */
class TestFragment: Fragment(R.layout.fg_test) {

    companion object{
        fun newInstance(): TestFragment {
            val args = Bundle()
            val fragment = TestFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<View>(R.id.btnFgOne).setOnClickListener {
            oneListener()
        }
        view.findViewById<View>(R.id.btnFgOne2).setOnClickListener {
            val bundle = Bundle()
            bundle.putString("key","这是 bundle 传值")
            context.startActivityForResultA(RouterPage.ROUTER_ONE,bundle){resultCode,data->
                Log.e("MMM", ":  --- $resultCode ---${data?.getStringExtra("result")}" );
            }
        }
        view.findViewById<View>(R.id.btnFgOne3).setOnClickListener {
            context.startActivityForResultA(RouterPage.ROUTER_ONE,
                "key" to "这是 pair 传值", "key2" to 88){resultCode,data->
                Log.e("MMM", ":  --- $resultCode ---${data?.getStringExtra("result")}" );
            }
        }
        view.findViewById<View>(R.id.btnFgOne4).setOnClickListener {
            ARouter.getInstance().build(RouterPage.ROUTER_ONE)
                .withString("key","keykeykey")
                .navigateForResult(this,99){requestCode, resultCode, data ->
                    Log.e("MMM", ": $requestCode --- $resultCode ---${data?.getStringExtra("result")}" );
                }
        }
    }

    fun oneListener(){
        context.startActivityA(RouterPage.ROUTER_ONE)
    }
}