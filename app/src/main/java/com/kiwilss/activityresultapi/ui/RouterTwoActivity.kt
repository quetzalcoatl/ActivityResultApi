package com.kiwilss.activityresultapi.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.alibaba.android.arouter.facade.annotation.Route
import com.kiwilss.activityresultapi.R
import com.kiwilss.activityresultapi.config.RouterPage

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/6
 * @desc   : {DESCRIPTION}
 */
@Route(path = RouterPage.ROUTER_TWO)
class RouterTwoActivity: AppCompatActivity(R.layout.activity_router_two) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction().add(R.id.flRouterTwoFl,TestFragment.newInstance()).commit()


    }
}