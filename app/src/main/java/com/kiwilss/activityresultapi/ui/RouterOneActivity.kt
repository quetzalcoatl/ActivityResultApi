package com.kiwilss.activityresultapi.ui

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.alibaba.android.arouter.facade.annotation.Route
import com.kiwilss.activityresultapi.R
import com.kiwilss.activityresultapi.config.RouterPage
import com.kiwilss.api.createIntent


/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/4
 * @desc   : {DESCRIPTION}
 */
@Route(path = RouterPage.ROUTER_ONE)
class RouterOneActivity: AppCompatActivity(R.layout.activity_router_one) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //使用 intent 获取参数
        val key = intent.getStringExtra("key")
        val key2 = intent.getIntExtra("key2",0)

        val tvText = findViewById<TextView>(R.id.tvRouterOneText)
        tvText.text = key

        Log.e("MMM", ": $key ----- $key2" );

    }

    fun finishListener(v: View){
        setResult(RESULT_OK,createIntent("result" to "hello"))
        finish()
    }
}