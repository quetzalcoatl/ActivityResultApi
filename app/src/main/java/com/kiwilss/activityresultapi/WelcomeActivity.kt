package com.kiwilss.activityresultapi

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/3
 * @desc   : {DESCRIPTION}
 */
class WelcomeActivity: AppCompatActivity(R.layout.activity_welcome) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tvText = findViewById<TextView>(R.id.tvText)

        tvText.text = intent.getStringExtra("text")

    }

    fun finishListener(view: View){
        //回传任意值
//        setResult(RESULT_OK,createIntent("result" to "hello"))
//        finish()
    }
}