package com.kiwilss.activityresultapi.config

/**
 *@author : Lss kiwilss
 * @e-mail : kiwilss@163.com
 * @time   : 2021/7/4
 * @desc   : {DESCRIPTION}
 */
object RouterPage {
    const val MAIN = "/kiwilss/main"
    const val ROUTER_ONE = "/kiwilss/router_one"
    const val ROUTER_TWO = "/kiwilss/router_two"

}