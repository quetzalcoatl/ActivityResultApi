package com.kiwilss.activityresultapi

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.alibaba.android.arouter.launcher.ARouter
import com.kiwilss.activityresultapi.config.RouterPage
import com.kiwilss.api.permission.requestMultiplePermissions
import com.kiwilss.api.permission.requestPermission
import com.kiwilss.api.permission.requestPermissionK
import com.kiwilss.api.permission.startActivityForResultK
import com.kiwilss.arouter.post.navigateForResult
import com.kiwilss.arouter.ys.startActivityA
import com.kiwilss.arouter.ys.startActivityForResultA


class MainActivity : AppCompatActivity(){
    val TAG = "MMM"
    val camera = Manifest.permission.CAMERA
    val write = Manifest.permission.WRITE_EXTERNAL_STORAGE
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //startActivityK<>()


    }

    private val startTest = startActivityForResultK {
        //这里是回调,相当于 onActivityResult
        if (it.resultCode == RESULT_OK) {
            //获取回传值,这里就是以前的 intent
            it.data?.run {
                Toast.makeText(
                    this@MainActivity,
                    "${getStringExtra("result")}", Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun testStart(view: View) {
        //调用跳转
        //startTest.launch(createIntentStart<WelcomeActivity>("text" to "这里可以传多个参数"))
    }

    //ktx 写法
    val multiple = requestMultiplePermissions(
        allGranted = {
            //全部同意
        },denied = {
            //拒绝
        },explained = {
            //拒绝并不再显示
        }
    )
    //dsl
    val multipleDsl = requestPermissionK {
        allGranted {  }
        denied {  }
        explained {  }
    }
    fun applyMultiple(view: View) {
//        multiple.launch(arrayOf(camera,write))
        multipleDsl.launch(arrayOf(camera,write))
    }


    //ktx 的用法
    val single = requestPermission(camera,
        granted = {
            //同意权限
        }, denied = {
            //拒绝权限
        }, explained = {
            //拒绝并且不再显示
        })

    // dsl 用法, 更好用更省事
    val singleDsl = requestPermissionK(camera) {
        //有两种写法,个人更喜欢下面这种不用写=,两个都写只有一个生效
        //granted = {}
        granted { /*同意权限*/ }
        denied { /*拒绝权限*/ }
        explained { /*拒绝并且不再显示*/ }
    }

    fun applySingle(view: View) {
        //调用
//        single.launch(camera)
        singleDsl.launch(camera)
    }

    //postcard 扩展使用
    fun aouterListener(view: View) {
        ARouter.getInstance().build(RouterPage.ROUTER_ONE)
            .withString("key","keykeykey")
            .navigateForResult(this,99){requestCode, resultCode, data ->
                Log.e(TAG, ": $requestCode --- $resultCode ---${data?.getStringExtra("result")}" );
            }
//        startActivity(Intent(this,RouterOneActivity::class.java))
    }
    fun aouterListener5(view: View) {
        startActivityA(RouterPage.ROUTER_TWO)
    }
    fun aouterListener2(view: View) {//无参
        startActivityForResultA(RouterPage.ROUTER_ONE){resultCode,data->
            Log.e(TAG, ":  --- $resultCode ---${data?.getStringExtra("result")}" );
        }
    }
    fun aouterListener3(view: View) {//bundle
        val bundle = Bundle()
        bundle.putString("key","这是 bundle 传值")
        startActivityForResultA(RouterPage.ROUTER_ONE,bundle){resultCode,data->
            Log.e(TAG, ":  --- $resultCode ---${data?.getStringExtra("result")}" );
        }
//        startActivityA(RouterPage.ROUTER_ONE,bundle)
    }
    fun aouterListener4(view: View) {//pair
        startActivityForResultA(RouterPage.ROUTER_ONE,
        "key" to "这是 pair 传值", "key2" to 88){resultCode,data->
            Log.e(TAG, ":  --- $resultCode ---${data?.getStringExtra("result")}" );
        }
//        startActivityA(RouterPage.ROUTER_ONE,"key" to "这是 pair 传值", "key2" to 88)
    }



}